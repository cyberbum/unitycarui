If you want to use this sound pack with "Realistic Engine Sounds" unity asset, you can download Lite version of that script here:
https://www.assetstore.unity3d.com/en/#!/content/94797

Import "Realistic Engine Sounds" asset package into your project and "RealisticEngineSounds_assets.unitypackage" to get prefabs for "Realistic Engine Sounds".