﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class Network : MonoBehaviour {

	private const string CAR_IP = "192.168.4.1"; // ESP8266 always assigns this IP
    //private const string CAR_IP = "192.168.1.11"; // On WIFI
    //private const string CAR_IP = "172.217.13.164"; // ESP8266 always assigns this IP
	private const int CAR_PORT = 80;

	private int _lastSentSpeed;
	private int _lastSentDirection;
    private int _lastSentVelocity;

    // https://stackoverflow.com/a/21642608 for good examples
	private TcpClient _client;
    private EConnectionState _connectionState;
    private Action<EConnectionState> _onConnectionStateChange;

	public enum EMotorId
	{
		MotorA,
		MotorB
	}

    public enum EConnectionState
    {
        Idle,
        Disconnected,
        Connecting,
        Connected,
    }

	private const string TagMotorAdjustA = "a";
    private const string TagMotorAdjustB = "b";
    private const string TagMotorSpeed = "s";
	private const string TagMessageEnd= "z";
    private const string TagMotorVelocity = "v";
    private const string TagMotorDirection = "d";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!_client.Connected && _connectionState != EConnectionState.Connecting)
        {
            _connectionState = EConnectionState.Connecting;
            Debug.Log("Lost connection to car, reconnecting...");
            StartCoroutine(Connect());
        }
	}

    private void Awake()
    {
        _client = new TcpClient();
    }

    private void OnDestroy()
    {
        _client.Close();
    }

    public bool IsConnected()
    {
        if (_client != null)
        {
            return _client.Connected;
        }

        return false;
    }

    public void ConnectToCar(Action<EConnectionState> onConnectionStateChange)
    {
        _onConnectionStateChange = onConnectionStateChange;
        StartCoroutine(Connect());
    }

	private IEnumerator Connect()
	{
		IAsyncResult asyncResult = _client.BeginConnect(CAR_IP, CAR_PORT, null, null);
        Debug.LogFormat("Trying to connect to car ({0})...", CAR_IP);
        _connectionState = EConnectionState.Connecting;
        _onConnectionStateChange.Invoke(_connectionState);

        while (!asyncResult.IsCompleted)
		{
			yield return new WaitForSeconds(1.0f);
            Debug.Log("Waiting for connection...");
		}

        Debug.Log("Connection completed");
        _client.EndConnect(asyncResult);

        if (!_client.Connected)
        {
            _connectionState = EConnectionState.Disconnected;
            _onConnectionStateChange.Invoke(_connectionState);    
        }
        else
        {
            _connectionState = EConnectionState.Connected;
            _onConnectionStateChange.Invoke(_connectionState);    
        }
	}

    public void SendMotorAdjustPacket(EMotorId motorId, int value)
    {
        if (!_client.Connected)
        {
            return;
        }

        string msg = String.Format("{0}{1}{2}", motorId == EMotorId.MotorA ? TagMotorAdjustA : TagMotorAdjustB, value, TagMessageEnd);
        StartCoroutine(SendPacket(msg));
    }

    public void SendVelocityPacket(int value)
    {
        if (!_client.Connected)
        {
            return;
        }

        if (_lastSentVelocity == value)
        {
            // Don't send if that's the last value we sent
            return;
        }

        _lastSentVelocity = value;

        string msg = String.Format("{0}{1}{2}", TagMotorVelocity, value, TagMessageEnd);
        StartCoroutine(SendPacket(msg));
    }

	public void SendSpeedPacket(int value)
	{
        if (!_client.Connected)
        {
            return;
        }

		if (_lastSentSpeed == value)
		{
			// Don't send if that's the last value we sent
			return;
		}
		
		_lastSentSpeed = value;

        string msg = String.Format("{0}{1}{2}", TagMotorSpeed, value, TagMessageEnd);
        StartCoroutine(SendPacket(msg));
	}

	public void SendCancelSpeedPacket(int value)
	{
        if (!_client.Connected)
        {
            return;
        }

		_lastSentSpeed = value;

        string msg = String.Format("{0}{1}{2}", TagMotorSpeed, value, TagMessageEnd);
        StartCoroutine(SendPacket(msg));
	}

	public void SendDirectionPacket(int value)
	{
        if (!_client.Connected)
        {
            return;
        }

		if (_lastSentDirection == value)
		{
			// Don't send if that's the last value we sent
			return;
		}
		
		_lastSentDirection = value;
		
        string msg = String.Format("{0}{1}{2}", TagMotorDirection, value, TagMessageEnd);
        StartCoroutine(SendPacket(msg));
	}

	public void SendCancelDirectionPacket(int value)
	{
        if (!_client.Connected)
        {
            return;
        }

		_lastSentDirection = value;
		
        string msg = String.Format("{0}{1}{2}", TagMotorDirection, value, TagMessageEnd);
        StartCoroutine(SendPacket(msg));
	}

    private IEnumerator SendPacket(string msg)
    {
        if (!_client.Connected)
        {
            yield break;
        }

        var stream = _client.GetStream();
        byte[] by = Encoding.ASCII.GetBytes(msg.ToCharArray(), 0, msg.Length);
        IAsyncResult result = stream.BeginWrite(by, 0, by.Length, null, null);

        while (!result.IsCompleted)
        {
            yield return new WaitForSeconds(0.0f);
        }

        Debug.LogFormat("Sent ({0}) bytes", by.Length);

        stream.Flush();
    }
}
