﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarController : MonoBehaviour {
    public GameObject StatusImageGo;
    public GameObject NetworkGo;
    public AudioSource HornSound;

    private Network _network;
    private Image _statusImage;

	// Use this for initialization
	void Start () {
        _network = NetworkGo.GetComponent<Network>();
        _statusImage = StatusImageGo.GetComponent<Image>();

        _network.ConnectToCar(OnConnectionStatus);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnHornPressed()
    {
        if (HornSound != null)
        {
            HornSound.Play();
        }
    }

    private void OnConnectionStatus(Network.EConnectionState connectionState)
    {
        switch(connectionState)
        {
            case Network.EConnectionState.Connecting:
                _statusImage.color = Color.yellow;
                break;
            case Network.EConnectionState.Connected:
                _statusImage.color = Color.green;
                break;
            case Network.EConnectionState.Disconnected:
                _statusImage.color = Color.red;
                break;
        }
    }
}
