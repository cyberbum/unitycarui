﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CarControlSlider : MonoBehaviour {
	public GameObject NetworkGo;

    public Text MotorAValueText;
    public Text MotorBValueText;

    public AudioSource IdleSound;
    public AudioSource LowSound;
    public AudioSource MedSound;
    public AudioSource HighSound;
    public AudioSource StopSound;

	private Network _network;
	private Slider _slider;
	
	private const int MAX_CAR_PARAM = 255;
	private const int RESET_CAR_SPEED = 128;
	
    private const int MAX_VELOCITY = 150;//255;
    private const int MIN_VELOCITY = 70;
    private const string MOTOR_A_PREF = "motor_a_adjust";
    private const string MOTOR_B_PREF = "motor_b_adjust";


    private const float HIGH_UPPER = 1.0f;
    private const float MED_UPPER = 0.86f;

    private const float DEADZONE_UPPER = 0.55f;

    private const float DEADZONE_LOWER = 0.45f;
    private const float LOW_LOWER = 0.26f;
    private const float MED_LOWER = 0.06f;
    private const float HIGH_LOWER = 0.0f;

	// Use this for initialization
	void Start () {
		_slider = GetComponent<Slider>();
		_network = NetworkGo.GetComponent<Network>();

        if (gameObject.name == "MotorAdjustA" && PlayerPrefs.HasKey(MOTOR_A_PREF))
        {
            _slider.value = PlayerPrefs.GetFloat(MOTOR_A_PREF);
            MotorAdjustAChange(_slider.value);
        }

        if (gameObject.name == "MotorAdjustB" && PlayerPrefs.HasKey(MOTOR_B_PREF))
        {
            _slider.value = PlayerPrefs.GetFloat(MOTOR_B_PREF);
            MotorAdjustBChange(_slider.value);
        }

        StartCoroutine(CoCheckIdle());
	}

    private void OnDestroy()
    {
        PlayerPrefs.Save();
    }

    IEnumerator CoCheckIdle()
    {
        while (true)
        {
            // If speed slider
            if (gameObject.name == "SpeedSlider")
            {
                if (IsSliderInDeadzone())
                {
                    // If deadzone for speed regularly cancel it
                    _network.SendCancelSpeedPacket(RESET_CAR_SPEED);        
                }
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    private void Update()
    {
        bool isConnected = true;
        // bool isConnected = _network.IsConnected();

        if (!isConnected)
        {
            IdleSound.Stop();
            LowSound.Stop();
            MedSound.Stop();
            HighSound.Stop();
        }

        if (IdleSound != null)
        {
            if (!IdleSound.isPlaying && IsSliderInDeadzone())
            {
                IdleSound.Play();
            }

            if (IdleSound.isPlaying && !IsSliderInDeadzone())
            {
                IdleSound.Stop();
            } 
        }

        if (LowSound != null)
        {
            if (!LowSound.isPlaying && IsSliderInLowValue())
            {
                LowSound.Play();
            }

            if (LowSound.isPlaying && !IsSliderInLowValue())
            {
                LowSound.Stop();
            } 
        }

        if (MedSound != null)
        {
            if (!MedSound.isPlaying && IsSliderInMedValue())
            {
                MedSound.Play();
            }

            if (MedSound.isPlaying && !IsSliderInMedValue())
            {
                MedSound.Stop();
            }
        }

        if (HighSound != null)
        {
            if (!HighSound.isPlaying && IsSliderInHighValue())
            {
                HighSound.Play();
            }

            if (HighSound.isPlaying && !IsSliderInHighValue())
            {
                HighSound.Stop();
            }
        }
    }

    private bool IsSliderInLowValue()
    {
        return _slider.value >= LOW_LOWER && _slider.value <= DEADZONE_LOWER;
    }

    private bool IsSliderInMedValue()
    {
        return _slider.value >= MED_LOWER && _slider.value <= LOW_LOWER;
    }

    private bool IsSliderInHighValue()
    {
        return _slider.value >= HIGH_LOWER && _slider.value <= MED_LOWER;
    }

    private bool IsSliderInDeadzone()
    {
        return (_slider.value >= DEADZONE_LOWER && _slider.value <= DEADZONE_UPPER);
    }


    public void MotorAdjustAChange(float value)
    {
        float percent = value * 100.0f;
        int remapValue = GetMotorAdjustValue((int)percent);
        MotorAValueText.text = remapValue.ToString();

        Debug.LogFormat("Sending motor A adjust value: {0}", remapValue);
        _network.SendMotorAdjustPacket(Network.EMotorId.MotorA, remapValue);

        PlayerPrefs.SetFloat(MOTOR_A_PREF, value);
    }

    public void MotorAdjustBChange(float value)
    {
        float percent = value * 100.0f;
        int remapValue = GetMotorAdjustValue((int)percent);
        MotorBValueText.text = remapValue.ToString();

        Debug.LogFormat("Sending motor B adjust value: {0}", remapValue);
        _network.SendMotorAdjustPacket(Network.EMotorId.MotorB, remapValue);

        PlayerPrefs.SetFloat(MOTOR_B_PREF, value);
    }

    public void VelocitySliderChange(float value)
    {
        float percent = value * 100.0f;
        int remapValue = GetRemapValue((int)percent, 0, 100, MIN_VELOCITY, MAX_VELOCITY);
            
        Debug.LogFormat("Sending velocity value: {0}", remapValue);
        _network.SendVelocityPacket(remapValue);
    }

	public void SpeedSliderChanged(float value)
	{
		//Debug.LogFormat ("Slider value: {0}", value);

		// Ignore dead zone
		if (IsSliderInDeadzone())
		{
			// Deadzone, ignore
			Debug.LogFormat("Speed slider in deadzone, ignore ({0})", value);
			return;
		}

		int speedValue = (int)GetCarValue(value);
		//Debug.LogFormat("Sending speed value: {0}", speedValue);
		_network.SendSpeedPacket(speedValue);
	}

	public void OnEndDragSpeedSlider(BaseEventData data)
    {
         Debug.Log("Speed drag end");
		 _slider.value = 0.5f;
		 _network.SendCancelSpeedPacket(RESET_CAR_SPEED);

        // Also cancel directional
        _network.SendCancelDirectionPacket(RESET_CAR_SPEED);
    }

	public void DirectionSliderChanged(float value)
	{
		//Debug.LogFormat ("Slider value: {0}", value);

		// Ignore dead zone
		if (IsSliderInDeadzone())
		{
			// Deadzone, ignore
			Debug.LogFormat("Direction slider in deadzone, ignore ({0})", value);
			return;
		}

		int directionValue = (int)GetCarValue(value);
		Debug.LogFormat("Sending direction value: {0}", directionValue);
		_network.SendDirectionPacket(directionValue);
	}

	public void OnEndDragDirectionSlider(BaseEventData data)
    {
         Debug.Log("Direction drag end");
		 _slider.value = 0.5f;
		 _network.SendCancelDirectionPacket(RESET_CAR_SPEED);
		 //_slider.transform.position = _sliderInitialPos;
    }

	private float GetCarValue(float value)
	{
		return value * MAX_CAR_PARAM;
	}
	
    private int GetRemapValue(int value, int fromMin, int fromMax, int toMin, int toMax)
    {
        return (((value - fromMin) * (toMax - toMin)) / (fromMax - fromMin)) + toMin;
    }

    private int GetMotorAdjustValue(int percent)
    {
        if (percent < 50)
        {
            return GetRemapValue(percent, 0, 49, -40, 0);
        }
        else if (percent > 50)
        {
            return GetRemapValue(percent, 51, 100, 0, 40);
        }

        return 0;
    }
}
