﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour {
    [SerializeField]
    Slider _speedSlider;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		// TODO Check if touches are below threshold near bottom of screen
		//  Check for touch inputs in screen and reposition slider under it?
		if (Input.touchCount > 0)
        {
            //Store the first touch detected.
            Touch myTouch = Input.touches[0];

			_speedSlider.transform.position = myTouch.position;
		}

		if (Input.GetMouseButtonDown(0))
        {
			Vector3 newPosition = new Vector3();

            Event e = Event.current;
			newPosition.x = Input.mousePosition.x;
            newPosition.y = Input.mousePosition.y;
        	_speedSlider.transform.position = newPosition;

            EventSystem.current.SetSelectedGameObject(_speedSlider.gameObject);
            PointerEventData mouse1 = EventSystem.current.gameObject.GetComponent<StandaloneInputModuleCustom>().GetLastPointerEventDataPublic(-1);
            _speedSlider.OnPointerDown(mouse1);
		}
	}

	/* 
	private void HideIfClickedOutside(GameObject panel) {
         if (Input.GetMouseButton(0) && panel.activeSelf && 
             !RectTransformUtility.RectangleContainsScreenPoint(
                 panel.GetComponent<RectTransform>(), 
                 Input.mousePosition, 
                 Camera.main)) {
             panel.SetActive(false);
         }
     }
	*/
}

