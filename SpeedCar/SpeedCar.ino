//--------------------------------------------------------------------------------------------------
// Racecar project by Georges Giroux, 2016
// Arduino code for wifi-controller rc car V1
// Base code adapted from here http://community.blynk.cc/t/robot-with-esp8266-shield-and-arduino-nano/3316
// Thanks!
//--------------------------------------------------------------------------------------------------

#include "WiFiEsp.h"
#include "SoftwareSerial.h"

//--------------------------------------------------------------------------------------------------
// Define this to 1 to enable FTDI prints
// Leave at 0 once you are done debugging, this has a performance impact.
//--------------------------------------------------------------------------------------------------
#define CAR_DEBUG 1

//--------------------------------------------------------------------------------------------------
// 1 to disable motor pins, 0 to enable
//--------------------------------------------------------------------------------------------------
#define DISABLE_MOTORS 0

//--------------------------------------------------------------------------------------------------
// Baud rate for the USB->serial and Arduino->hard serial
//--------------------------------------------------------------------------------------------------
#define BAUD_RATE 115200

//--------------------------------------------------------------------------------------------------
// Port on which the server listens to
//--------------------------------------------------------------------------------------------------
#define LISTEN_PORT 80

//--------------------------------------------------------------------------------------------------
// You access point's name and password. This is what you'll connect your phone to communicate with
//--------------------------------------------------------------------------------------------------
#define AP_SSID "cycar" // your network SSID (name)
#define AP_PASS "thisisasuckycar" // your network password

#define JOIN_SSID "cyberlink"
#define JOIN_PASS "booboo"

bool useAP = true; // true-> access point mode, or else try to join a network

//--------------------------------------------------------------------------------------------------
// Wifi channel to use, tweak this to lower interference!
//--------------------------------------------------------------------------------------------------
//#define AP_CHANNEL 12
//#define AP_CHANNEL 5
//#define AP_CHANNEL 8
#define AP_CHANNEL 1

//--------------------------------------------------------------------------------------------------
// Encryption mode to use. Values taken from EspDrv.h
//--------------------------------------------------------------------------------------------------
/*
 // Encryption modes
  enum wl_enc_type {
  ENC_TYPE_NONE = 0,
  ENC_TYPE_WEP = 1,
  ENC_TYPE_WPA_PSK = 2,
  ENC_TYPE_WPA2_PSK = 3,
  ENC_TYPE_WPA_WPA2_PSK = 4
};
 */
 #define AP_ENCRYPTION_MODE 3

//--------------------------------------------------------------------------------------------------
// State variables
//--------------------------------------------------------------------------------------------------
int status = WL_IDLE_STATUS;      // the Wifi radio's status
boolean alreadyConnected = false; // whether or not the client was connected previously
String message;                   // Buffer containing received data

WiFiEspServer server(LISTEN_PORT);

//--------------------------------------------------------------------------------------------------
// Motor values
//--------------------------------------------------------------------------------------------------
int motorA = 0;
int motorB = 0;

int motorTweakA = 0; // Adjustments to help make car go straight properly
int motorTweakB = 0;

const int DEFAULT_MOTOR_IDLE = 128;
const int MOTOR_MAX = 255;
const int ADJUSTOR_MAX = 60;
const int ADJUSTOR_FORWARD_MIN = 200;
const int ADJUSTOR_BACKWARD_MIN = 55;

//--------------------------------------------------------------------------------------------------
// Input values received over the network
//--------------------------------------------------------------------------------------------------
int X = DEFAULT_MOTOR_IDLE;
int Y = DEFAULT_MOTOR_IDLE;

int factor=0;
int maxSpeed = 70;
int adjustor = 0;

const int factorValueForwards = 5;
const int factorValueBackwards = 5;

//--------------------------------------------------------------------------------------------------
// Digital pins
//--------------------------------------------------------------------------------------------------
const int STBY    = 4;  // standby pin for motor driver
const int PWMA    = 3;  // Speed control motor A
const int AIN1    = 5;  // Direction motor A
const int AIN2    = 6;  // Direction motor A

const int PWMB    = 9;  // Speed control motor B
const int BIN1    = 10; // Direction motor B
const int BIN2    = 11; // Direction motor B

const int LEDSU   = 2;  // Pin for led status (success)

const int SOFT_RX = 7;  // Software serial receive (blue wire)
const int SOFT_TX = 8;  // Software serial transmit (green wire)

//--------------------------------------------------------------------------------------------------
// Software serial, with pins. Connect the pins from your FTDI to the Arduino pins
//--------------------------------------------------------------------------------------------------
SoftwareSerial Ftdi(SOFT_RX, SOFT_TX);

//--------------------------------------------------------------------------------------------------
// Set an alias for our ESP serial, so we don't make mistakes in code below
//--------------------------------------------------------------------------------------------------
#define EspSerial Serial

//--------------------------------------------------------------------------------------------------
// FTDI print statements, are cleared out according to debug define
//--------------------------------------------------------------------------------------------------
#if CAR_DEBUG
#define FTDI_PRINT Ftdi.print
#define FTDI_PRINTLN Ftdi.println
#else
#define FTDI_PRINT
#define FTDI_PRINTLN
#endif

//--------------------------------------------------------------------------------------------------
// Activate and deactivate led. Mainly used to indicate car is ready
//--------------------------------------------------------------------------------------------------
void activateLed()
{
  digitalWrite(LEDSU, HIGH);
}

void deactivateLed()
{
  digitalWrite(LEDSU, LOW);
}

//--------------------------------------------------------------------------------------------------
// Setup: prepare serial comms, motor pings and wifi
//--------------------------------------------------------------------------------------------------
void setup()
{
  Ftdi.begin(BAUD_RATE);
  delay(10);
  
  EspSerial.begin(BAUD_RATE);
  delay(10);

  FTDI_PRINTLN("Setting up output pins...");

  pinMode(LEDSU, OUTPUT);

#if !DISABLE_MOTORS
  pinMode(STBY, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(AIN2, OUTPUT);
  
  pinMode(PWMB, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(BIN2, OUTPUT);

  // Disable standy on motors right away
  digitalWrite(STBY, HIGH); 
#endif // DISABLE_MOTORS

  deactivateLed();
  setupWifi();
}

//--------------------------------------------------------------------------------------------------
// Setup wifi
//--------------------------------------------------------------------------------------------------
void setupWifi()
{
  FTDI_PRINTLN("Setting up wifi...");
  
  // initialize ESP module
  WiFi.init(&EspSerial);

  // check for the presence of the shield
  while (WiFi.status() == WL_NO_SHIELD) 
  {
    FTDI_PRINTLN("WiFi shield not present, retrying...");
    // don't continue
    delay(1000);
  }

  if (useAP)
  {
    FTDI_PRINTLN("Creating local AP...");
    status = WiFi.beginAP(AP_SSID, AP_CHANNEL, AP_PASS, AP_ENCRYPTION_MODE);
    if (!status)
    {
      FTDI_PRINTLN("Failed to start local AP");
  
      // don't continue
      while (true);
    }
  }
  else
  {
    FTDI_PRINTLN("Joining network...");
    status = WiFi.begin(JOIN_SSID, JOIN_PASS);
    if (!status)
    {
      FTDI_PRINTLN("Failed to join local network");
  
      // don't continue
      while (true);
    }
  }
  
  FTDI_PRINTLN("You're connected to the network");
  printWifiStatus();

  // Start the web server
  FTDI_PRINTLN("Starting web server...");
  server.begin();

  if (server.isStarted())
  {
    activateLed();
  }
}

//--------------------------------------------------------------------------------------------------
// Main loop
//--------------------------------------------------------------------------------------------------
void loop()
{
  processNetworkTcp();

#if !DISABLE_MOTORS
  processMotors();
#endif // DISABLE_MOTORS
}

//--------------------------------------------------------------------------------------------------
// Process all received and updated values for the motors
//--------------------------------------------------------------------------------------------------
void processMotors()
{
  if (X == 128 && Y == 128)  //  No movement
  {
    motorA = 0;
    motorB = 0;

    analogWrite(PWMA, motorA);
    analogWrite(PWMB, motorB);

    digitalWrite(AIN1, HIGH);  
    digitalWrite(AIN2, LOW);
    
    digitalWrite(BIN1, HIGH);
    digitalWrite(BIN2, LOW);  
  } 
   
  if (X >= 129 && Y == 128)   // Forward
  {
    motorA = X;
    motorB = X;
    
    motorA = map(motorA, 129, MOTOR_MAX, 70, maxSpeed);

    analogWrite(PWMA, motorA + motorTweakA);
    digitalWrite(AIN1, HIGH);
    digitalWrite(AIN2, LOW);
    
    motorB = map(motorB, 129, MOTOR_MAX, 70, maxSpeed);
    analogWrite(PWMB, motorB + motorTweakB);
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, HIGH);
  }
   
  if (X >= 129 && Y <= 127)   // Right forward
  {
    motorA = X;
    motorB = X;
    factor = Y;

    factor = map(factor, 0, 127, factorValueForwards, 0);

    adjustor = 0;
    if (X > ADJUSTOR_FORWARD_MIN)
    {
      // Adjust according to speed, more speed, more adjustment, faster reaction
      adjustor = map(X, 129, MOTOR_MAX, 0, ADJUSTOR_MAX);
    }
    
    // Add more power so it turns more tightly
    motorA = map(motorA, 129, MOTOR_MAX, 70, maxSpeed);
    motorA = constrain(motorA + factor + motorTweakB + adjustor, 0, MOTOR_MAX);
    analogWrite(PWMA, motorA);
    digitalWrite(AIN1, HIGH);
    digitalWrite(AIN2, LOW);
    
    motorB = map(motorB, 129, MOTOR_MAX, 70, maxSpeed);
    motorB = constrain(motorB - factor - motorTweakB - adjustor, 0, MOTOR_MAX);
    analogWrite(PWMB, motorB);
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, HIGH);
  }

  // Left turn is slow compared to right turn
  if(X >= 129 && Y >= 129)   // Left forward
  {
    motorA = X;
    motorB = X;
    factor = Y;
    factor = map(factor, 129, MOTOR_MAX, 0, factorValueForwards);
    
    adjustor = 0;
    if (X > ADJUSTOR_FORWARD_MIN)
    {
      // Adjust according to speed, more speed, more adjustment, faster reaction
      adjustor = map(X, 129, MOTOR_MAX, 0, ADJUSTOR_MAX);
    }

    motorA = map(motorA, 129, MOTOR_MAX, 70, maxSpeed);
    motorA = constrain(motorA - factor - motorTweakA - adjustor, 0, MOTOR_MAX);
    analogWrite(PWMA, motorA);
    digitalWrite(AIN1, HIGH);
    digitalWrite(AIN2, LOW);
    
    motorB = map(motorB, 129, MOTOR_MAX, 70, maxSpeed);
    motorB = constrain(motorB + factor + motorTweakA + adjustor, 0, MOTOR_MAX);
    analogWrite(PWMB, motorB);
    digitalWrite(BIN1, LOW);
    digitalWrite(BIN2, HIGH);
  }
  
  if (X <= 127 && Y == 128)   // Backwards
  {
    motorA = X;
    motorB = X;

    // Motor A pins
    motorA = map(motorA, 0, 126, maxSpeed, 70);
    analogWrite(PWMA, motorA);
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, HIGH);

    // Motor B pins
    motorB = map(motorB, 0, 126, maxSpeed, 70);
    analogWrite(PWMB, motorB);
    digitalWrite(BIN1, HIGH);
    digitalWrite(BIN2, LOW);
  }

  if (X <= 127 && Y <= 127)   // Reverse right
  {
    motorA = X;
    motorB = X;
    factor = Y;
    factor = map(factor, 0, 127, factorValueBackwards, 0);

    adjustor = 0;
    if (X < ADJUSTOR_BACKWARD_MIN)
    {
      // Adjust according to speed, more speed, more adjustment, faster reaction
      adjustor = map(X, 0, 127, ADJUSTOR_MAX, 0);
    }
    
    motorA = map(motorA, 0, 126, maxSpeed, 70);
    motorA = constrain(motorA + factor + motorTweakB + adjustor, 0, MOTOR_MAX);
    analogWrite(PWMA, motorA);
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, HIGH);
    
    motorB = map(motorB, 0, 126, maxSpeed, 70);
    motorB = constrain(motorB - factor - motorTweakB - adjustor, 0, MOTOR_MAX);
    analogWrite(PWMB, motorB);
    digitalWrite(BIN1, HIGH);
    digitalWrite(BIN2, LOW);
  }
   
  if (X <= 127 && Y >= 129)   // Reverse left
  {
    motorA = X;
    motorB = X;
    factor = Y;
    factor = map(factor, 129, MOTOR_MAX, 0, factorValueBackwards);
    
    adjustor = 0;
    if (X < ADJUSTOR_BACKWARD_MIN)
    {
      // Adjust according to speed, more speed, more adjustment, faster reaction
      adjustor = map(X, 0, 127, ADJUSTOR_MAX, 0);
    }

    motorA = map(motorA, 0, 126, maxSpeed, 70);
    motorA = constrain(motorA - factor - motorTweakA - adjustor, 0, MOTOR_MAX);
    analogWrite(PWMA, motorA);
    digitalWrite(AIN1, LOW);
    digitalWrite(AIN2, HIGH);
    
    motorB = map(motorB, 0, 126, maxSpeed, 70);
    motorB = constrain(motorB + factor + motorTweakA + adjustor, 0, MOTOR_MAX);
    analogWrite(PWMB, motorB);
    digitalWrite(BIN1, HIGH);
    digitalWrite(BIN2, LOW);
  }
}

//--------------------------------------------------------------------------------------------------
// Process all tcp packets we receive
//--------------------------------------------------------------------------------------------------
void processNetworkTcp()
{
  // listen for incoming clients
  WiFiEspClient client = server.available();
  if (client) 
  {
      if (!alreadyConnected) 
      {
        // clean out the input buffer:
        client.flush();    
        FTDI_PRINTLN("We have a new client");
        alreadyConnected = true;
        message = "";
      } 

      int bytesAvailable = client.available();
      if (bytesAvailable > 0) 
      {
        // read the bytes incoming from the client:
        char thisChar = client.read();
        if (thisChar == 'z')
        {
          // Finish buffer, print it
          FTDI_PRINT("Got buffer:");
          FTDI_PRINTLN(message);

          if (message[0] == 's') // Forward speed
          {
            int X1 = atoi(&message[1]);
            X = constrain(X1, 0, MOTOR_MAX);

            FTDI_PRINT("Got new X value: ");
            FTDI_PRINTLN(X, DEC);
          }
          else if (message[0] == 'd') // Direction
          {
            int Y1 = atoi(&message[1]);
            Y = constrain(Y1, 0, MOTOR_MAX);
          
            FTDI_PRINT("Got new Y value: ");
            FTDI_PRINTLN(Y, DEC);
          }
          else if (message[0] == 'v') // Velocity
          {
            int vel = atoi(&message[1]);
            maxSpeed = constrain(vel, 0, MOTOR_MAX);

            FTDI_PRINT("Got new maxSpeed value: ");
            FTDI_PRINTLN(maxSpeed, DEC);
          }
          else if (message[0] == 'a') // Motor A adjust
          {
            motorTweakA = atoi(&message[1]);
            FTDI_PRINT("Got new motor tweak A value: ");
            FTDI_PRINTLN(motorTweakA, DEC);
          }
          else if (message[0] == 'b') // Motor B adjust
          {
            motorTweakB = atoi(&message[1]);
            FTDI_PRINT("Got new motor tweak B value: ");
            FTDI_PRINTLN(motorTweakB, DEC);
          }
          message = "";
        }
        else
        {
          message += thisChar;
        }
      }
  }
}

//--------------------------------------------------------------------------------------------------
// Printout wifi status
//--------------------------------------------------------------------------------------------------
void printWifiStatus()
{
  // print the SSID of the network you're attached to
  FTDI_PRINT("SSID: ");
  FTDI_PRINTLN(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  FTDI_PRINT("IP Address: ");
  FTDI_PRINTLN(ip);
}





